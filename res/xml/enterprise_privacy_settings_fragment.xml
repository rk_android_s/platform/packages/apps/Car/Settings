<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (C) 2021 The Android Open Source Project
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<PreferenceScreen
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:settings="http://schemas.android.com/apk/res-auto"
    android:title="@string/enterprise_privacy_settings"
    android:key="@string/psk_enterprise_privacy_settings">

<!--  TODO(b/206022572): preferences below were copied mostly "as-is" from phone, they need to be
      polished for automotive (for example, adding dividers, removing order, etc...) -->

    <PreferenceCategory android:key="@string/psk_enterprise_privacy_exposure_category"
                        android:order="200"
                        android:title="@string/enterprise_privacy_exposure_category"
                        android:contentDescription="@string/enterprise_privacy_exposure_category">
        <Preference android:key="@string/pk_enterprise_privacy_enterprise_data"
                    android:order="210"
                    android:layout_height="wrap_content"
                    android:title="@string/enterprise_privacy_enterprise_data"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_installed_packages"
                    android:order="220"
                    android:title="@string/enterprise_privacy_installed_packages"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_usage_stats"
                    android:order="230"
                    android:title="@string/enterprise_privacy_usage_stats"
                    android:selectable="false"/>
    <!-- TODO(b/206155858): figure out why it doesn't have a title -->
        <Preference android:key="@string/pk_enterprise_privacy_network_logs"
                    settings:controller="com.android.car.settings.enterprise.NetworkLogsPreferenceController"
                    android:order="240"
                    android:title="@string/enterprise_privacy_network_logs"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_bug_reports"
                    settings:controller="com.android.car.settings.enterprise.BugReportsPreferenceController"
                    android:order="250"
                    android:title="@string/enterprise_privacy_bug_reports"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_security_logs"
                    settings:controller="com.android.car.settings.enterprise.SecurityLogsPreferenceController"
                    android:order="260"
                    android:title="@string/enterprise_privacy_security_logs"
                    android:selectable="false"/>
    </PreferenceCategory>

    <PreferenceCategory android:title="@string/enterprise_privacy_exposure_changes_category"
                        android:order="300"
                        android:key="@string/psk_enterprise_privacy_exposure_changes_category">
        <Preference
                    settings:controller="com.android.car.settings.enterprise.EnterpriseInstalledPackagesPreferenceController"
                    android:order="310"
                    android:key="@string/pk_enterprise_privacy_number_enterprise_installed_packages"
                    android:title="@string/enterprise_privacy_enterprise_installed_packages"/>
        <Preference
                    settings:controller="com.android.car.settings.enterprise.AdminGrantedLocationPermissionsPreferenceController"
                    android:order="320"
                    android:key="@string/pk_enterprise_privacy_number_location_access_packages"
                    android:title="@string/enterprise_privacy_location_access"/>
        <Preference
                    settings:controller="com.android.car.settings.enterprise.AdminGrantedMicrophonePermissionPreferenceController"
                    android:order="330"
                    android:key="@string/pk_enterprise_privacy_number_microphone_access_packages"
                    android:title="@string/enterprise_privacy_microphone_access"/>
        <Preference
                    settings:controller="com.android.car.settings.enterprise.AdminGrantedCameraPermissionPreferenceController"
                    android:order="340"
                    android:key="@string/pk_enterprise_privacy_number_camera_access_packages"
                    android:title="@string/enterprise_privacy_camera_access"/>
        <Preference
                    settings:controller="com.android.car.settings.enterprise.EnterpriseSetDefaultAppsPreferenceController"
                    android:order="350"
                    android:key="@string/pk_enterprise_privacy_number_enterprise_set_default_apps"
                    android:title="@string/enterprise_privacy_enterprise_set_default_apps"/>

	<!-- TODO(b/206155695): figure out why it doesn't have a title -->
        <Preference android:key="@string/pk_enterprise_privacy_always_on_vpn_primary_user"
                    settings:controller="com.android.car.settings.enterprise.AlwaysOnCurrentUserPreferenceController"
                    android:order="360"
                    android:selectable="false"/>

        <Preference android:key="@string/pk_enterprise_privacy_input_method"
                    settings:controller="com.android.car.settings.enterprise.ImePreferenceController"
                    android:order="380"
                    android:title="@string/enterprise_privacy_input_method"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_global_http_proxy"
                    settings:controller="com.android.car.settings.enterprise.GlobalHttpProxyPreferenceController"
                    android:order="390"
                    android:title="@string/enterprise_privacy_global_http_proxy"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_ca_certs_current_user"
                    settings:controller="com.android.car.settings.enterprise.CaCertsCurrentUserPreferenceController"
                    android:order="400"
                    android:title="@string/enterprise_privacy_ca_certs_personal"
                    android:selectable="false"/>
    </PreferenceCategory>

    <PreferenceCategory android:key="@string/psk_enterprise_privacy_device_access_category"
                        android:order="500"
                        android:title="@string/enterprise_privacy_device_access_category">
        <Preference android:key="@string/pk_enterprise_privacy_lock_device"
                    android:order="510"
                    android:title="@string/enterprise_privacy_lock_device"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_wipe_device"
                    settings:controller="com.android.car.settings.enterprise.WipeDevicePreferenceController"
                    android:order="520"
                    android:title="@string/enterprise_privacy_wipe_device"
                    android:selectable="false"/>
        <Preference android:key="@string/pk_enterprise_privacy_failed_password_wipe_current_user"
                    settings:controller="com.android.car.settings.enterprise.FailedPasswordWipeCurrentUserPreferenceController"
                    android:order="530"
                    android:title="@string/enterprise_privacy_failed_password_wipe_device"
                    android:selectable="false"/>
    </PreferenceCategory>

    <com.android.settingslib.widget.FooterPreference
        android:key="@string/pk_enterprise_privacy_header"
        android:title="@string/enterprise_privacy_header"
        android:selectable="false"
        settings:searchable="false"/>
</PreferenceScreen>
